class Animal {
     move(distanceInMeters: number = 0) {
        console.log(`Animal moved ${distanceInMeters}m.`);
    }
}

class Dog extends Animal {
  nom: string;
    bark() {
      console.log('Woof! Woof!');
      console.log("mon nom est: " + this.nom);
        //this.move(10);
    }
  setNom() {
    this.nom = "toto";
  }
}

const dog = new Dog();
dog.bark();
dog.move(10);
dog.bark();
dog.setNom();
dog.bark();
//dog = new Dog(); //produit erreur
