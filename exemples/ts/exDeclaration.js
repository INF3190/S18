// voir https://www.typescriptlang.org/docs/handbook/variable-declarations.html
// tester avec la commande:
// ts-node exDeclaration.ts
function f(shouldInitialize) {
    if (shouldInitialize) {
        var x = 10; // tester var, let, const
    }
    return x;
}
console.log(f(true)); // returne '10'
//console.log(f(false)); // returne 'undefined'
// erreur dans tous les cas si let ou const
