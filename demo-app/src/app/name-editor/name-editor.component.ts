import { Component, OnInit } from '@angular/core';
// ajout formulaire
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-name-editor',
  templateUrl: './name-editor.component.html',
  styleUrls: ['./name-editor.component.css']
})
export class NameEditorComponent implements OnInit {
  
  nomcomplet = "";
  name = new FormControl(''); // ajout form
  prenom = new FormControl('Jean');
  
  updateName() {

  this.nomcomplet = this.prenom.value+" " + this.name.value;

  this.name.setValue('Nancy');
  this.prenom.setValue('Jean');

   }
  constructor() { }

  ngOnInit(): void {
  }
  
}
