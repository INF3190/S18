import { Component } from '@angular/core';
//importer classe formulaire reactifs
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Page illustration formulaire sous Angular';
  name = new FormControl();
  firstname = new FormControl();
  readonly donnees ={"name":"Jean","firstname":"Baptiste"};
  clear(){
    this.name.setValue('');
  }

  reset(){
    this.name.setValue(this.donnees.name);
    this.firstname.setValue(this.donnees.firstname);
  }
}
